<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogPostsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('blog_posts', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->uuid('bid')->nullable()->default(NULL);      // blog id
          $table->foreign('bid')->references('id')->on('blogs')->onDelete('set null');
          $table->string('post');
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('blog_posts');
    }
}
