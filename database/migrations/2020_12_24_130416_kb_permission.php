<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KbPermission extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      Schema::create('kb_permissions', function (Blueprint $table) {
        $table->uuid('id')->primary();
        $table->uuid('kbid')->nullable()->default(NULL);
        $table->foreign('kbid')->references('id')->on('kb_articles')->onDelete('set null');
        $table->uuid('uid')->nullable()->default(NULL);
        $table->foreign('uid')->references('id')->on('users')->onDelete('set null');
        $table->boolean('viewAny')->default(FALSE);
        $table->boolean('view')->default(FALSE);
        $table->boolean('create')->default(FALSE);
        $table->boolean('update')->default(FALSE);
        $table->boolean('delete')->default(FALSE);
        $table->boolean('restore')->default(FALSE);
        $table->boolean('forceDelete')->default(FALSE);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::dropIfExists('kb_permissions');
    }
}
