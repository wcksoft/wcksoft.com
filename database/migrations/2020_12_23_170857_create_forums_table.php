<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('forums', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->string('name');
          $table->text('description')->nullable()->default(NULL);
          $table->uuid('parent_id')->nullable()->default(NULL);
          //$table->foreign('parent_id')->references('id')->on('forums')->onDelete('set null');
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
          $table->uuid('locked_by')->nullable()->default(NULL);
          $table->foreign('locked_by')->references('id')->on('users')->onDelete('set null');
          $table->timestamp('locked_at')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('forums');
    }
}
