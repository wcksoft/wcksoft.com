<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKbArticlesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('kb_articles', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->uuid('kbcid'); // kb category id
          $table->uuid('pid')->nullable()->default(NULL); // product id
          $table->uuid('ftid')->nullable()->default(NULL); // assigned to issue first seen in forum topic id
          $table->uuid('tid')->nullable()->default(NULL); // assigned to issue first seen in ticket id
          //$table->char('title', 128);
          //$table->string('article');
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->uuid('deleted_by')->nullable()->default(NULL);
          //$table->uuid('published_by')->nullable()->default(NULL);
          //$table->timestamp('published_at')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('kb_articles');
    }
}
