<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketUploadsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ticket_uploads', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->uuid('uid');  // user id
          $table->uuid('tid'); // ticket id
          $table->char('name', 128);
          $table->binary('data');
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('ticket_uploads');
    }
}
