<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tickets', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->uuid('tcid'); // ticket category id
          $table->uuid('kbid')->nullable()->default(NULL); // kb id
          $table->char('title', 128);
          $table->string('description');
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->uuid('closed_by')->nullable()->default(NULL);
          $table->timestamp('closed_at')->nullable()->default(NULL);
          $table->uuid('reopened_by')->nullable()->default(NULL);
          $table->timestamp('reopened_at')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tickets');
    }
}
