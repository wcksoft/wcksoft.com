<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->string('firstname');
          $table->string('lastname');
          $table->index(['firstname', 'lastname']);
          $table->string('email');
          $table->index('email');
          $table->timestamp('email_verified_at')->nullable();
          $table->string('password');
          $table->boolean('admin')->default(FALSE);
          $table->binary('picture')->nullable()->default(NULL);
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->rememberToken();
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
