<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BlogPostPermission extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      Schema::create('bp_permissions', function (Blueprint $table) {
        $table->uuid('id')->primary();
        $table->uuid('bpid')->nullable()->default(NULL);
        $table->foreign('bpid')->references('id')->on('blog_posts')->onDelete('set null');
        $table->uuid('uid')->nullable()->default(NULL);
        $table->foreign('uid')->references('id')->on('users')->onDelete('set null');
        $table->boolean('viewAny')->default(FALSE);
        $table->boolean('view')->default(FALSE);
        $table->boolean('create')->default(FALSE);
        $table->boolean('update')->default(FALSE);
        $table->boolean('delete')->default(FALSE);
        $table->boolean('restore')->default(FALSE);
        $table->boolean('forceDelete')->default(FALSE);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::dropIfExists('bp_permissions');
    }
}
