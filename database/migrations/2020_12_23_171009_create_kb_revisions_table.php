<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKbRevisionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('kb_revisions', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->uuid('kbid'); // kb id
          $table->char('title', 128);
          $table->string('article');
          $table->string('version')->nullable()->default(NULL);
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->uuid('published_by')->nullable()->default(NULL);
          $table->timestamp('published_at')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('kb_revisions');
    }
}
