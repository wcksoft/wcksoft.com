<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumUploadsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('forum_uploads', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->string('name')->nullable()->default(NULL);
          $table->uuid('fpid')->nullable()->default(NULL);
          $table->foreign('fpid')->references('id')->on('forum_posts')->onDelete('set null');
          $table->binary('file')->nullable()->default(NULL);
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->rememberToken();
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('forum_uploads');
    }
}
