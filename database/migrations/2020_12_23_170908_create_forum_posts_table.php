<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumPostsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('forum_posts', function (Blueprint $table) {
          $table->uuid('id')->primary();
          $table->text('post');
          $table->uuid('tid')->nullable()->default(NULL);
          $table->foreign('tid')->references('id')->on('forum_topics')->onDelete('set null');
          $table->uuid('created_by')->nullable()->default(NULL);
          $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
          $table->uuid('deleted_by')->nullable()->default(NULL);
          $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
          $table->uuid('updated_by')->nullable()->default(NULL);
          $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
          $table->boolean('show')->default(TRUE);   // when post needs validation, this will be FALSE
          $table->uuid('approved_by')->nullable()->default(NULL);
          $table->foreign('approved_by')->references('id')->on('users')->onDelete('set null');
          $table->timestamp('approved_at')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('forum_posts');
    }
}
