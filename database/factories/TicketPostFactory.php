<?php

namespace Database\Factories;

use App\Models\TicketPost;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketPostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketPost::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
