<?php

namespace Database\Factories;

use App\Models\ForumUpload;
use Illuminate\Database\Eloquent\Factories\Factory;

class ForumUploadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ForumUpload::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
