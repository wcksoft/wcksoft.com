<?php

namespace Database\Factories;

use App\Models\KbUpload;
use Illuminate\Database\Eloquent\Factories\Factory;

class KbUploadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = KbUpload::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
