<?php

namespace Database\Factories;

use App\Models\TicketUpload;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketUploadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketUpload::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
