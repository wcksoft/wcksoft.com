<?php

namespace Database\Factories;

use App\Models\KbRevision;
use Illuminate\Database\Eloquent\Factories\Factory;

class KbRevisionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = KbRevision::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
