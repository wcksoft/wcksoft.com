// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gray: colors.trueGray,
        teal: colors.teal,
        cyan: colors.cyan,
        lightblue: colors.lightBlue,
        indigo: colors.indigo,
        wckblue: {
          50: '#e7e7fd',
          100: '#b8b8fa',
          200: '#8888f6',
          300: '#5959f3',
          400: '#2929ef',
          500: '#1010d6',
          600: '#0c0ca6',
          700: '#090977',
          800: '#050547',
          900: '#050547',
        },
        wckcyan: {
          50: '#e5fbff',
          100: '#b3f2ff',
          200: '#80e9ff',
          300: '#4de1ff',
          400: '#1ad8ff',
          500: '#00bfe6',
          600: '#0094b3',
          700: '#006a80',
          800: '#00404d',
          900: '#00151a',
        },
        greyblue: {
          50: '#edf3f8',
          100: '#c8daea',
          200: '#a3c2db',
          300: '#7ea9cd',
          400: '#5a91bf',
          500: '#4077a5',
          600: '#325d81',
          700: '#24425c',
          800: '#152837',
          900: '#070d12',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
