<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.include.meta', $meta??[])
        <title>@yield('title')WckCA</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="stylesheet" href="{{ asset('css/ca.css') }}">
        <script src="{{ asset('js/ca.js') }}" defer></script>
        <script src="https://kit.fontawesome.com/ba7373fa35.js" crossorigin="anonymous"></script>
        @stack('head')
    </head>
    <body class="font-sans antialiased">
        <div class="flex flex-col min-h-screen bg-gray-100 justify-between">

            <main class="pt-14 pb-1 flex">

              <div class="flex-grow">
                
                @yield('content')
              </div>
            </main>
            @include('layouts.include.footer')
        </div>
        @stack('modals')
        @stack('body')
    </body>
</html>
