<footer class="h-6">
  <div class="w-full text-center">
    &copy; 2020 Wicked Software Corporation
  </div>
</footer>
