<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="robots" content="{{$meta['robots']??'index,follow'}}">
<meta name="description" content="{{$meta['description']??'Check how you can be the office hero and save lots of precious time by using our digital certificate tools for free!'}}">
<meta name="author" content="{{$meta['author']??'Wicked Software Corporation'}}">
@if($meta['created']??FALSE)<meta name="created" content="{{$meta['created']}}">@endif
<meta property="og:type" content="{{$meta['type']??'website'}}">
<meta property="og:title" content="{{$meta['title']??'WckCA'}}">
<meta property="og:description" content="{{$meta['description']??'Check how you can be the office hero and save lots of precious time by using our digital certificate tools for free!'}}">
<meta property="og:image" content="{{$meta['image']??url('static/images/social.jpg')}}">
<meta property="og:url" content="{{$meta['url']??'https://ca.wckosft.com/'}}">
<meta property="og:site_name" content="Covid19 Real Data">
<meta property="fb:admins" content="100000875125543">
<meta property="fb:app_id" content="2557514967848835">
<meta name="twitter:title" content="{{$meta['title']??'WckCA'}}">
<meta name="twitter:description" content="{{$meta['description']??'Check how you can be the office hero and save lots of precious time by using our digital certificate tools for free!'}}">
<meta name="twitter:image" content="{{$meta['image']??url('static/images/social-t.jpg')}}">
@if($meta['site']??FALSE)<meta name="twitter:site" content="{{$meta['site']}}">@endif
@if($meta['creator']??FALSE)<meta name="twitter:creator" content="{{$meta['creator']}}">@endif
<meta name="twitter:image:alt" content="WckCA Logo">
