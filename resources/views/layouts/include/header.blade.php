@if(View::hasSection('header'))
  <header class="bg-white shadow mt-2 w-3/5 mx-auto rounded-3xl">
      <div class="py-6 px-4 sm:px-6 lg:px-8">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight text-center">
          @yield('header')
        </h2>
      </div>
  </header>
@endif
