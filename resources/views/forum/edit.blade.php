@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>{{$forum->name}}</h3>

      @include('errors')

      {{ Form::model($forum, ['route' => ['forums.update', $forum->id], 'method' => "PUT"]) }}

        <div class="form-group row">
          {{ Form::label('name', 'Forum name', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::text('name', $forum->name, ['placeholder' => "Name of the new forum.",
                                         'class' => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('description', 'Description', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::textarea('description', $forum->description, ['placeholder' => "Enter a brief description of this forum.",
                                                    'class' => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('parent_id', 'Parent forum', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::select('parent_id', $parent_forums, $forum->parent_id, ['class' => "form-control"]) }}
          </div>
        </div>

        <div>
          {{ Form::submit('Save Forum', ['class' => "btn btn-outline-primary"]) }}
          <a class="btn btn-outline-danger btn-sm" href="/forums">Cancel</a>
        </div>

      {{ Form::close() }}
    </div>
  </main>
@endsection
