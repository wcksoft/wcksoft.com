<table class="w-100">
  @include('forum.head')
  <tbody>
    @foreach ($forums as $forum)
      @include('forum.row')
    @endforeach
  </tbody>
</table>
