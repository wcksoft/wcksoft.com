<tr>
  <td class="align-middle">
    @if ($forum->locked_at != NULL)
      <i class="c-red-500 ti-lock"></i>
    @endif
    <a href='/forums/{{$forum->id}}'>{{$forum->name}}</a>
  </td>
  <td class="d-none d-sm-table-cell align-middle">{{$forum->topics->count()}}</td>
  <td class="d-none d-sm-table-cell align-middle">{{$forum->posts->count()}}</td>
  <td class="align-middle">
    @if ($forum->posts->last() === NULL)
      <span>No Posts.</span>
    @else
      <span class="d-block">By {{ $forum->lastestPostAuthor() }}</span>
      <span class="d-block">On {{ $forum->lastestPostTime() }}<span>
    @endif
  </td>
  <td class="pr-0 align-middle">
    @include('forum.actions')
  </td>
</tr>
