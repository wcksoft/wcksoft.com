@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3 class="mb-0">{{ $current->name }}</h3>
      <h6>{!! $current->showLinks() !!}</h6>
      @include('errors')
      <div class="align-items-start flex-column shadow mb-20 table">
        @if ($forums->count())
          @include('forum.forums')
        @endif
      </div>
      <div class="align-items-start flex-column shadow mb-20 table">
        @if ($topics->count())
          @include('forum.topic.topics')
        @endif
        @if ($topics->count() < 1 && $forums->count() < 1)
          <div class="alert alert-danger" role="alert">
            No topics in this forums.
          </div>
        @endif
      </div>
      @include('forum.button')
      @include('forum.topic.button')
    </div>
  </main>
@endsection
