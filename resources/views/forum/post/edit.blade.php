@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('head')
  <script src="https://cdn.tiny.cloud/1/9wga4t28ul69ngi3k7jslrgtmfhuy3ee4g4ceitidngbiakq/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>Editing post from {{$topic->title}}</h3>

      @include('errors')

      {{ Form::model($post, ['route' => ['posts.update', $post->id], 'method' => "PUT"]) }}

        <div class="form-group row">
          {{ Form::label('post', 'Post', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::textarea('post', $post->post, ['placeholder' => "Enter a post for this topic.",
                                                    'class' => "form-control forum-post",
                                                    'id' => "post-edit"]) }}
          </div>
        </div>

        <div>
          {{ Form::submit('Save Post', ['class' => "btn btn-outline-primary"]) }}
          <a class="btn btn-outline-danger btn-sm" href="/forums">Cancel</a>
        </div>

      {{ Form::close() }}
    </div>
  </main>
@endsection

@section('bottom')
  <script>
      tinymce.init({
          selector:'textarea#post-edit',
      });
  </script>
@endsection
