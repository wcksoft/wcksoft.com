@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>Showing post from {{$topic->title}}</h3>
      <h6>{!! $post->showLinks() !!}</h6>
      @include('errors')
      <table class="w-100">
        <tbody>
          @include('forum.post.main')
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2">
              <a href="{{route('posts.edit', ['post' => $post])}}" class="btn btn-outline-primary">
                <i class="ti-pencil"></i> Edit
              </a>
              <a href="{{route('posts.destroy', ['post' => $post])}}" class="btn btn-outline-primary">
                <i class="ti-pencil"></i> Delete
              </a>
            </td>
          <tr>
        </tfoot>
      </table>
    </div>
  </main>
@endsection
