@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('head')
  <script src="https://cdn.tiny.cloud/1/9wga4t28ul69ngi3k7jslrgtmfhuy3ee4g4ceitidngbiakq/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>Reply to {{$topic->title}}</h3>

      @include('errors')

      {{ Form::open(['route' => ['topics.posts.store', $topic->id]]) }}

        <div class="form-group row">
          <div class="col-sm-12">
            {{ Form::textarea('post', null, ['placeholder' => "Enter a post for this topic.",
                                             'class' => "form-control forum-post",
                                             'id' => "topic-reply"]) }}
          </div>
        </div>

        <div>
          {{ Form::submit('Post Reply', ['class' => "btn btn-outline-primary"]) }}
          <a class="btn btn-outline-danger btn-sm" href="/forums">Cancel</a>
        </div>

      {{ Form::close() }}
    </div>
  </main>
@endsection

@section('bottom')
  <script>
      tinymce.init({
          selector:'textarea#topic-reply',
      });
  </script>
@endsection
