@can('create', $post)
  <a href="{{route('topics.posts.create', ['topic' => $post['tid']])}}" class="btn btn-outline-primary">
    <i class="ti-comment-alt"></i> Add Reply
  </a>
@endcan
