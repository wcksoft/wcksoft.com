@if (is_null($current??NULL))
  @can('create', App\Forum::class)
    <a href="{{route('forums.create')}}" class="btn btn-outline-primary">
      <i class="ti-plus"></i> Add Forum
    </a>
  @endcan
@else
  @can('create', App\Forum::class)
    <a href="{{route('forums.forum.create', ['forum' => $current->id])}}" class="btn btn-outline-primary">
      <i class="ti-plus"></i> Add Forum
    </a>
  @endcan
@endif
