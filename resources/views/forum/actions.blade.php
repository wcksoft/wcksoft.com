<div class="d-flex flex-row justify-content-end">
  @can('update', $forum)
    <div class="p-2">
      <a href="{{route('forums.edit', ['forum' => $forum->id])}}">
        <i class="c-blue-500 ti-pencil"></i>
      </a>
    </div>
  @endcan

  @if ($forum->locked_at == NULL)
    @can('lock', $forum)
      <div class="p-2">
        {{ Form::open(['route' => array('forums.lock', $forum->id),
                       'id' => "forum_lock_".explode('-', $forum->id)[0],
                       'method' => "PUT"])}}
          <a href="#" onclick="document.getElementById('forum_lock_{{explode('-', $forum->id)[0]}}').submit()">
            <i class="c-blue-500 ti-lock"></i>
          </a>
        {{ Form::close() }}
      </div>
    @endcan
  @else
    @can('unlock', $forum)
      <div class="p-2">
        {{ Form::open(['route' => array('forums.unlock', $forum->id),
                       'id' => "forum_unlock_".explode('-', $forum->id)[0],
                       'method' => "PUT"])}}
             <a href="#" onclick="document.getElementById('forum_unlock_{{explode('-', $forum->id)[0]}}').submit()">
               <i class="c-blue-500 ti-unlock"></i>
             </a>
        {{ Form::close() }}
      </div>
    @endcan
  @endif
  
  @can('delete', $forum)
    <div class="p-2">
      {{ Form::open(['route' => array('forums.destroy', $forum->id),
                     'id' => "forum_del_".explode('-', $forum->id)[0],
                     'method' => "DELETE"])}}
           <a href="#" onclick="document.getElementById('forum_del_{{explode('-', $forum->id)[0]}}').submit()">
             <i class="c-red-500 ti-trash"></i>
           </a>
      {{ Form::close() }}
    </div>
  @endcan
</div>
