@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>Create a new forum.</h3>

      @include('errors')

      @if (is_null($forum))
        {{ Form::open(['route' => ['forums.store']]) }}
      @else
        {{ Form::open(['route' => ['forums.forum.store', 'forum' => $forum->id]]) }}
      @endif

        <div class="form-group row">
          {{ Form::label('name', 'Forum name', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::text('name', null, ['placeholder' => "Name of the new forum.",
                                         'class' => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('description', 'Description', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::textarea('description', null, ['placeholder' => "Enter a brief description of this forum.",
                                                    'class' => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('parent_id', 'Parent forum', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            @if (is_null($forum))
              {{ Form::select('parent_id', $parent_forums, null, ['class' => "form-control"]) }}
            @else
              {{ Form::select('parent_id', $parent_forums, $forum->id, ['class' => "form-control"]) }}
            @endif
          </div>
        </div>

        <div>
          {{ Form::submit('Create Forum', ['class' => "btn btn-outline-primary"]) }}
          <a class="btn btn-outline-danger btn-sm" href="/forums">Cancel</a>
        </div>

      {{ Form::close() }}
    </div>
  </main>
@endsection
