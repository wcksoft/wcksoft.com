@forelse ($categories as $cat)
  <div class="align-items-start flex-column shadow mb-20 table">
    <div class="d-flex flex-row">
      <div class="p-2">
        @if ($cat->locked_at != NULL)
          <i class="c-red-500 ti-lock"></i>
        @endif
      </div>
      <div class="p-2 flex-fill text-center font-weight-bold text-truncate">
        <a href="{{url('/forums/'.$cat->id)}}" class="text-decoration-none">
          {{$cat->name}}
        </a>
      </div>
      @include('forum.category.actions')
    </div>
    @include('forum.category.forums')
  </div>
@empty
  <div class="alert alert-danger" role="alert">
    No categories found.
  </div>
@endforelse
