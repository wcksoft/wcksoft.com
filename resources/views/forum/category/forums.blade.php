@if ($cat->forums()->count())
  <table class="w-100">
    @include('forum.head')
    <tbody>
      @foreach ($cat->forums() as $forum)
        @include('forum.row')
      @endforeach
    </tbody>
  </table>
@else
  @if ($cat->topics()->count())
    @include('forum.category.topics')
  @else
    <div class="d-flex flex-row border-top text-center">
      <div class="alert alert-danger w-100 mT-10" role="alert">
        No forums in this category.
      </div>
    </div>
  @endif
@endif
