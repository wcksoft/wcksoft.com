<table class="w-100">
  @include('forum.topic.head')
  <tbody>
    @foreach ($cat->topics()->get() as $topic)
      @include('forum.topic.row')
    @endforeach
  </tbody>
</table>
