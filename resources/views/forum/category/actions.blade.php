<div class="p-2 ml-auto">
  @can('update', $cat)
    <a href="{{route('forums.edit', ['forum' => $cat->id])}}">
      <i class="c-blue-500 ti-pencil"></i>
    </a>
  @endcan
</div>

@if ($cat->locked_at == NULL)
  @can('lock', $cat)
    <div class="p-2">
      {{ Form::open(['route' => array('forums.lock', $cat->id),
                     'id' => "forum_lock_".explode('-', $cat->id)[0],
                     'method' => "PUT"])}}
        <a href="#" onclick="document.getElementById('forum_lock_{{explode('-', $cat->id)[0]}}').submit()">
          <i class="c-blue-500 ti-lock"></i>
        </a>
      {{ Form::close() }}
    </div>
  @endcan
@else
  @can('unlock', $cat)
    <div class="p-2">
      {{ Form::open(['route' => array('forums.unlock', $cat->id),
                   'id' => "forum_unlock_".explode('-', $cat->id)[0],
                   'method' => "PUT"])}}
         <a href="#" onclick="document.getElementById('forum_unlock_{{explode('-', $cat->id)[0]}}').submit()">
           <i class="c-blue-500 ti-unlock"></i>
         </a>
      {{ Form::close() }}
    </div>
  @endcan
@endif

@can('delete', $cat)
  <div class="p-2">
    {{ Form::open(['route' => array('forums.destroy', $cat->id),
                 'id' => "forum_del_".explode('-', $cat->id)[0],
                 'method' => "DELETE"])}}
       <a href="#" onclick="document.getElementById('forum_del_{{explode('-', $cat->id)[0]}}').submit()">
         <i class="c-red-500 ti-trash"></i>
       </a>
    {{ Form::close() }}
  </div>
@endcan
