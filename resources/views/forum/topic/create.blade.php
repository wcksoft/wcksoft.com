@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('head')
  <script src="https://cdn.tiny.cloud/1/9wga4t28ul69ngi3k7jslrgtmfhuy3ee4g4ceitidngbiakq/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>Create a new topic.</h3>
      @include('errors')

      @if (is_null($forum))
        {{ Form::open(['route' => ['topics.store']]) }}
      @else
        {{ Form::open(['route' => ['topics.forum.create',
                       'forum' => $forum->id]]) }}
      @endif

        <div class="form-group row">
          {{ Form::label('title', 'Topic name', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::text('title', null, ['placeholder' => "Name of the new topic.",
                                          'class' => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-12">
            {{ Form::textarea('post', null, ['placeholder' => "Enter a post for this topic.",
                                             'class' => "form-control forum-post",
                                             'id' => "topic-create"]) }}
          </div>
        </div>

        @if (is_null($forum))
          <div class="form-group row">
            {{ Form::label('fid', 'Parent forum', ['class' => "col-sm-2 col-form-label"]) }}
            <div class="col-sm-10">
              {{ Form::select('fid', $parent_forums, null, ['placeholder' => '--- Pick a parent forum ---',
                                                            'class' => "form-control"]) }}
            </div>
          </div>
        @else
          {{ Form::hidden('fid', $forum->id) }}
        @endif

        <div>
          {{ Form::submit('Create Topic', ['class' => "btn btn-outline-primary"]) }}
          <a class="btn btn-outline-danger btn-sm" href="/forums">Cancel</a>
        </div>

      {{ Form::close() }}

    </div>
  </main>
@endsection

@section('bottom')
  <script>
      tinymce.init({
          selector: 'textarea#topic-create',
          auto_focus: 'topic-create',
          menubar: false,
          elementpath: false,
          height: "400",
          resize: false,
          plugins: "bbcode autolink link code emoticons image preview",
          bbcode_dialect: "punbb",
          default_link_target: "_blank",
          image_dimensions: false,
          image_description: false,
          image_uploadtab: false,
          hidden_input: false,
          toolbar_mode: 'wrap',
          toolbar: 'forecolor backcolor bold italic underline | emoticons link image | alignleft aligncenter alignright alignjustify | outdent indent | preview code | undo redo'
      });
  </script>
@endsection
