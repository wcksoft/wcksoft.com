<tr>
  <td class="align-middle">
    <a href='/topics/{{$topic->id}}'>
      @if ($topic->locked_at != NULL)
        <i class="c-red-500 ti-lock"></i>
      @endif
      @if ($topic->pinned == TRUE)
        <i class="c-red-500 ti-pin2"></i>
      @endif
      @if ($topic->important == TRUE)
        <i class="c-red-700 ti-flag-alt"></i>
      @endif
      {{$topic->title}}
    </a>
  </td>
  <td class="d-none d-sm-table-cell align-middle">{{$topic->authorName()}}</td>
  <td class="d-none d-sm-table-cell align-middle">{{$topic->posts->count()}}</td>
  <td class="align-middle">
    @if ($topic->posts->last() === NULL)
      <span>No Posts.</span>
    @else
      <span class="d-block">By {{ $topic->lastestPostAuthor() }}</span>
      <span class="d-block">On {{ $topic->lastestPostTime() }}<span>
    @endif
  </td>
  <td class="pr-0 align-middle">
    @include('forum.topic.actions')
  </td>
</tr>
