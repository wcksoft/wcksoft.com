@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>List of all topics.</h3>
      @include('errors')
      <div class="align-items-start flex-column shadow mb-20 table">
        @if ($topics->count())
          @include('forum.topic.topics')
        @else
          <table class="w-100">
            <tbody>
              <tr>
                <td colspan='6' class="alert alert-danger">There is no topics.</td>
              </tr>
            </tbody>
          </table>
        @endif
      </div>
      @include('forum.topic.button')
    </div>
  </main>
@endsection
