<table class="w-100">
  @include('forum.topic.head')
  <tbody>
    @foreach ($topics as $topic)
      @include('forum.topic.row')
    @endforeach
  </tbody>
</table>
