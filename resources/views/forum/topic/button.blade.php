@if (is_null($current??NULL))
  @can('create', App\Topic::class)
    <a href="{{route('topics.create')}}" class="btn btn-outline-primary">
      <i class="ti-plus"></i> Add Topic
    </a>
  @endcan
@else
  @can('create', App\Topic::class)
    <a href="{{route('topics.forum.create', ['forum' => $current->id])}}" class="btn btn-outline-primary">
      <i class="ti-plus"></i> Add Topic
    </a>
  @endcan
@endif
