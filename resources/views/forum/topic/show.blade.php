@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>{{$topic->title}}</h3>
      <h6>{!! $topic->showLinks() !!}</h6>
      @include('errors')
      <div class="align-items-start flex-column shadow mb-20 table">
        @if ($posts->count())
          <table class="w-100 forum-posts">
            <tbody>
              @foreach ($posts as $post)
                @include('forum.post.main')
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2">
                  {{ $posts->links() }}
                </td>
              <tr>
            </tfoot>
          </table>
        @else
          <div class="alert alert-danger" role="alert">
            No posts found in this topic.
          </div>
        @endif
      </div>
      @include('forum.topic.lock')
      @include('forum.post.reply')
    </div>
  </main>
@endsection
