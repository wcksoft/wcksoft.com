@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>Edit {{$topic->title}}</h3>

      @include('errors')

      {{ Form::model($topic, ['route' => ['topics.update', $topic->id], 'method' => "PUT"]) }}

        <div class="form-group row">
          {{ Form::label('title', 'Topic name', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::text('title', $topic->title, ['placeholder' => "Name of the new topic.",
                                                   'class' => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('fid', 'Parent forum', ['class' => "col-sm-2 col-form-label"]) }}
          <div class="col-sm-10">
            {{ Form::select('fid', $parent_forums, $topic->parent_id, ['placeholder' => '--- Pick a parent forum ---',
                                                                       'class' => "form-control"]) }}
          </div>
        </div>

        <div>
          {{ Form::submit('Create Topic', ['class' => "btn btn-outline-primary"]) }}
          <a class="btn btn-outline-danger btn-sm" href="/forums">Cancel</a>
        </div>

      {{ Form::close() }}

    </div>
  </main>
@endsection
