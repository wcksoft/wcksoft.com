<div class="d-flex flex-row justify-content-end">
  @can('update', $topic)
    <div class="p-2">
      <a href="{{route('topics.edit', ['topic' => $topic->id])}}">
        <i class="c-blue-500 ti-pencil"></i>
      </a>
    </div>
  @endcan

  @if ($topic->locked_at == NULL)
    @can('lock', $topic)
      <div class="p-2">
        {{ Form::open(['route' => array('topics.lock', $topic->id),
                       'id' => "topic_lock_".explode('-', $topic->id)[0],
                       'method' => "PUT"])}}
          <a href="#" onclick="document.getElementById('topic_lock_{{explode('-', $topic->id)[0]}}').submit()">
            <i class="c-blue-500 ti-lock ti-na"></i>
          </a>
        {{ Form::close() }}
      </div>
    @endcan
  @else
    @can('unlock', $topic)
      <div class="p-2">
        {{ Form::open(['route' => array('topics.unlock', $topic->id),
                       'id' => "topic_unlock_".explode('-', $topic->id)[0],
                       'method' => "PUT"])}}
           <a href="#" onclick="document.getElementById('topic_unlock_{{explode('-', $topic->id)[0]}}').submit()">
             <i class="c-blue-500 ti-unlock"></i>
           </a>
        {{ Form::close() }}
      </div>
    @endcan
  @endif

  @if ($topic->pinned)
    @can('unpin', $topic)
      <div class="p-2">
        {{ Form::open(['route' => array('topics.unpin', $topic->id),
                       'id' => "topic_unpin_".explode('-', $topic->id)[0],
                       'method' => "PUT"])}}
          <a href="#" onclick="document.getElementById('topic_unpin_{{explode('-', $topic->id)[0]}}').submit()">
            <i class="c-blue-500 ti-pin2"></i>
          </a>
        {{ Form::close() }}
      </div>
    @endcan
  @else
    @can('pin', $topic)
      <div class="p-2">
        {{ Form::open(['route' => array('topics.pin', $topic->id),
                       'id' => "topic_pin_".explode('-', $topic->id)[0],
                       'method' => "PUT"])}}
           <a href="#" onclick="document.getElementById('topic_pin_{{explode('-', $topic->id)[0]}}').submit()">
             <i class="c-blue-500 ti-pin2"></i>
           </a>
        {{ Form::close() }}
      </div>
    @endcan
  @endif

  @if ($topic->important)
    @can('unflag', $topic)
      <div class="p-2">
        {{ Form::open(['route' => array('topics.unflag', $topic->id),
                       'id' => "topic_unflag_".explode('-', $topic->id)[0],
                       'method' => "PUT"])}}
          <a href="#" onclick="document.getElementById('topic_unflag_{{explode('-', $topic->id)[0]}}').submit()">
            <i class="c-blue-500 ti-lock ti-na"></i>
          </a>
        {{ Form::close() }}
      </div>
    @endcan
  @else
    @can('flag', $topic)
      <div class="p-2">
        {{ Form::open(['route' => array('topics.flag', $topic->id),
                       'id' => "topic_flag_".explode('-', $topic->id)[0],
                       'method' => "PUT"])}}
           <a href="#" onclick="document.getElementById('topic_flag_{{explode('-', $topic->id)[0]}}').submit()">
             <i class="c-blue-500 ti-flag-alt"></i>
           </a>
        {{ Form::close() }}
      </div>
    @endcan
  @endif

  @can('delete', $topic)
    <div class="p-2">
      {{ Form::open(['route' => array('topics.destroy', $topic->id),
                     'id' => "topic_del_".explode('-', $topic->id)[0],
                     'method' => "DELETE"])}}
           <a href="#" onclick="document.getElementById('topic_del_{{explode('-', $topic->id)[0]}}').submit()">
             <i class="c-red-500 ti-trash"></i>
           </a>
      {{ Form::close() }}
    </div>
  @endcan
</div>
