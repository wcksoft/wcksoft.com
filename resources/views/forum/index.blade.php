@extends('adminator.main', $meta??[])
@section('title', "Forum - Covid19 Real Data")

@section('menu')
  @include('adminator.side.menu', ['menu' => "forum"])
@endsection

@section('content')
  <main class='main-content bgc-grey-100'>
    <div id='mainContent'>
      <h3>Welcome to Covid19's bulletin board.</h3>
      @include('errors')
      @include('forum.category.list')
      @include('forum.button')
    </div>
  </main>
@endsection
