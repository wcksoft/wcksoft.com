@extends('blog.layout.blog')

@section('content')
  <div class="blog-landing">
    <div class="box1">
      <div>
        <h1>Wicked Software's Blog</h1>
        <h5>Real talk about real tech.</h5>
      </div>
    </div>
    <div class="box2">
      <div>
        <h5>Built on</h5>
        <h1>wckBlog technology</h1>
      </div>
    </div>
    <div class="box3">
      <div>
        <h1>Currently no blogs</h1>
        <h5>in the space time continuum</h5>
        <h3><i class="fas fa-space-shuttle"></i></h3>
      </div>
    </div>
    <div class="box4">
      <div>
        <a href="/blog"><h2><i class="far fa-list-alt"></i> Check all blogs</h2></a>
      </div>
    </div>
  </div>
@endsection
