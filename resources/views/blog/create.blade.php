@extends('blog.layout.blog')

@section('content')
  <div class="banner">
    Wcksoft's Blog
  </div>
  <div class="blog">
    <div>
      create a blog
    </div>
    <div>
      @include('blog.layout.menu.social')
      @include('blog.layout.menu.cta')
      @include('blog.layout.menu.blogs')
    </div>
  </div>
@endsection
