<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      @include('meta', $meta??[])

      <title>Wcksoft's blog</title>

      <!-- Scripts -->
      <script src="{{ asset('js/wck.js') }}" defer></script>
      @yield('scripts')

      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
      <script src="https://kit.fontawesome.com/ba7373fa35.js" crossorigin="anonymous"></script>

      <!-- Styles -->
      <link href="{{ asset('css/wck.css') }}" rel="stylesheet">
      <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
      @yield('css')
  </head>
  <body>
    <div class="wck" id="wck">
      @yield('content')
    </div>
  </body>
</html>
