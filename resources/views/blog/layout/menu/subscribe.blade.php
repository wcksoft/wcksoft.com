<div class="subs mb-3">
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text" id="subs-add"><i class="fas fa-envelope"></i></span>
    </div>
    <input type="email" class="form-control" placeholder="email@address.com" aria-label="subs" aria-describedby="subs-add">
  </div>
  <button class="btn btn-secondary">Subscribe</button>
</div>
