<div>
  <div class="social mb-3">
    <div>Share on</div>
    <i class="fab fa-facebook-square fa-2x fb"></i>
    <i class="fab fa-twitter-square fa-2x twitter"></i>
    <i class="fab fa-linkedin fa-2x linkedin"></i>
    <i class="fas fa-rss-square fa-2x rss"></i>
    <i class="fas fa-envelope-square fa-2x"></i>
  </div>
  @include('blog.layout.menu.subscribe')
</div>
