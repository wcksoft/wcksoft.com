@extends('blog.layout.blog')

@section('content')
  <div class="banner">
    Wcksoft's Blog
  </div>
  <div class="blog">
    <div>
      @forelse ($blogs as $blog)
          <li>{{ $user->title }}</li>
      @empty
          <p>No blogs</p>
      @endforelse
    </div>
    <div>
      @include('blog.layout.menu.index.social')
      @include('blog.layout.menu.index.cta')
      @include('blog.layout.menu.blogs')
    </div>
  </div>
@endsection
