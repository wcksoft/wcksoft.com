<?php

namespace App\Http\Controllers;

use App\Models\KbRevision;
use Illuminate\Http\Request;

class KbRevisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KbRevision  $kbRevision
     * @return \Illuminate\Http\Response
     */
    public function show(KbRevision $kbRevision)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KbRevision  $kbRevision
     * @return \Illuminate\Http\Response
     */
    public function edit(KbRevision $kbRevision)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KbRevision  $kbRevision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KbRevision $kbRevision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KbRevision  $kbRevision
     * @return \Illuminate\Http\Response
     */
    public function destroy(KbRevision $kbRevision)
    {
        //
    }
}
