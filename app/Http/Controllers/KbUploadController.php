<?php

namespace App\Http\Controllers;

use App\Models\KbUpload;
use Illuminate\Http\Request;

class KbUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KbUpload  $kbUpload
     * @return \Illuminate\Http\Response
     */
    public function show(KbUpload $kbUpload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KbUpload  $kbUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(KbUpload $kbUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KbUpload  $kbUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KbUpload $kbUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KbUpload  $kbUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(KbUpload $kbUpload)
    {
        //
    }
}
