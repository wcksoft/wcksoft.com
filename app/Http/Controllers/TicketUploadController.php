<?php

namespace App\Http\Controllers;

use App\Models\TicketUpload;
use Illuminate\Http\Request;

class TicketUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TicketUpload  $ticketUpload
     * @return \Illuminate\Http\Response
     */
    public function show(TicketUpload $ticketUpload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TicketUpload  $ticketUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(TicketUpload $ticketUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TicketUpload  $ticketUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TicketUpload $ticketUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TicketUpload  $ticketUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(TicketUpload $ticketUpload)
    {
        //
    }
}
