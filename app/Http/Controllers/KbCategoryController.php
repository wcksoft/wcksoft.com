<?php

namespace App\Http\Controllers;

use App\Models\KbCategory;
use Illuminate\Http\Request;

class KbCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KbCategory  $kbCategory
     * @return \Illuminate\Http\Response
     */
    public function show(KbCategory $kbCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KbCategory  $kbCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(KbCategory $kbCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KbCategory  $kbCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KbCategory $kbCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KbCategory  $kbCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(KbCategory $kbCategory)
    {
        //
    }
}
