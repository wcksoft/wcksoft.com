<?php

namespace App\Http\Controllers;

use App\Models\ForumUpload;
use Illuminate\Http\Request;

class ForumUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ForumUpload  $forumUpload
     * @return \Illuminate\Http\Response
     */
    public function show(ForumUpload $forumUpload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ForumUpload  $forumUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(ForumUpload $forumUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ForumUpload  $forumUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ForumUpload $forumUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ForumUpload  $forumUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(ForumUpload $forumUpload)
    {
        //
    }
}
