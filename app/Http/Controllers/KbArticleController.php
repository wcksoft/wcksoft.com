<?php

namespace App\Http\Controllers;

use App\Models\KbArticle;
use Illuminate\Http\Request;

class KbArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KbArticle  $kbArticle
     * @return \Illuminate\Http\Response
     */
    public function show(KbArticle $kbArticle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KbArticle  $kbArticle
     * @return \Illuminate\Http\Response
     */
    public function edit(KbArticle $kbArticle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KbArticle  $kbArticle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KbArticle $kbArticle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KbArticle  $kbArticle
     * @return \Illuminate\Http\Response
     */
    public function destroy(KbArticle $kbArticle)
    {
        //
    }
}
