<?php

namespace App\Policies;

use App\Models\Forum;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ForumPolicy {
    use HandlesAuthorization;

    public function before(User $user, $ability) {
        if ($user->admin) { return TRUE; }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user) {
      $access = DB::table('forum_permissions')
                   ->select('viewAny')
                   ->where('uid', $user->id)
                   ->where('fid', NULL)
                   ->get();
      if (count($access)) { return $access[0]['viewAny']; }
      return TRUE;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum  $forum
     * @return mixed
     */
    public function view(User $user, Forum $forum) {
      $access = DB::table('forum_permissions')
                   ->select('view')
                   ->where('uid', $user->id)
                   ->where('fid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['view']) { return $access[0]['view']; } }
      unset($access);
      $access = DB::table('forum_permissions')
                   ->select('view')
                   ->where('uid', $user->id)
                   ->where('fid', $forum->id)
                   ->get();
      if (count($access)) { return $access[0]['view']; }
      if ($forum->locked_at === NULL) {
        return TRUE;
      } else {
        return FALSE;
      }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user) {
      $access = DB::table('forum_permissions')
                   ->select('create')
                   ->where('uid', $user->id)
                   ->where('fid', NULL)
                   ->get();
      if (count($access)) { return $access[0]['create']; }
       return FALSE;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum  $forum
     * @return mixed
     */
    public function update(User $user, Forum $forum) {
      if ($user->id == $forum->created_by) { return TRUE; }
      $access = DB::table('forum_permissions')
                   ->select('update')
                   ->where('uid', $user->id)
                   ->where('fid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['update']) { return $access[0]['update']; } }
      unset($access);
      $access = DB::table('forum_permissions')
                   ->select('update')
                   ->where('uid', $user->id)
                   ->where('fid', $forum->id)
                   ->get();
      if (count($access)) { return $access[0]['update']; }
      return FALSE;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum  $forum
     * @return mixed
     */
    public function delete(User $user, Forum $forum) {
      $access = DB::table('forum_permissions')
                   ->select('delete')
                   ->where('uid', $user->id)
                   ->where('fid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['delete']) { return $access[0]['delete']; } }
      unset($access);
      $access = DB::table('forum_permissions')
                   ->select('delete')
                   ->where('uid', $user->id)
                   ->where('fid', $forum->id)
                   ->get();
      if (count($access)) { return $access[0]['delete']; }
      return FALSE;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum  $forum
     * @return mixed
     */
    public function restore(User $user, Forum $forum) {
      $access = DB::table('forum_permissions')
                   ->select('restore')
                   ->where('uid', $user->id)
                   ->where('fid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['restore']) { return $access[0]['restore']; } }
      unset($access);
      $access = DB::table('forum_permissions')
                   ->select('restore')
                   ->where('uid', $user->id)
                   ->where('fid', $forum->id)
                   ->get();
      if (count($access)) { return $access[0]['restore']; }
      return FALSE;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forum  $forum
     * @return mixed
     */
    public function forceDelete(User $user, Forum $forum) {
      $access = DB::table('forum_permissions')
                   ->select('forceDelete')
                   ->where('uid', $user->id)
                   ->where('fid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['forceDelete']) { return $access[0]['forceDelete']; } }
      unset($access);
      $access = DB::table('forum_permissions')
                   ->select('forceDelete')
                   ->where('uid', $user->id)
                   ->where('fid', $forum->id)
                   ->get();
      if (count($access)) { return $access[0]['forceDelete']; }
      return FALSE;
    }
}
