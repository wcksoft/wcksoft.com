<?php

namespace App\Policies;

use App\Models\KbRevision;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class KbRevPolicy {
    use HandlesAuthorization;

    public function before(User $user, $ability) {
        if ($user->admin) { return TRUE; }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user) {
      $access = DB::table('kbrev_permissions')
                   ->select('viewAny')
                   ->where('uid', $user->id)
                   ->where('krid', NULL)
                   ->get();
      if (count($access)) { return $access[0]['viewAny']; }
      return TRUE;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\KbRevision  $kbRevision
     * @return mixed
     */
    public function view(User $user, KbRevision $kbRevision) {
      if ($user->id == $rev->created_by) { return TRUE; }
      if ($blog->status != "published") { return FALSE; }
      $access = DB::table('kbrev_permissions')
                   ->select('view')
                   ->where('uid', $user->id)
                   ->where('krid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['view']) { return $access[0]['view']; } }
      unset($access);
      $access = DB::table('kbrev_permissions')
                   ->select('view')
                   ->where('uid', $user->id)
                   ->where('krid', $rev->id)
                   ->get();
      if (count($access)) { return $access[0]['view']; }
      return TRUE;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user) {
      $access = DB::table('kbrev_permissions')
                   ->select('create')
                   ->where('uid', $user->id)
                   ->where('krid', NULL)
                   ->get();
      if (count($access)) { return $access[0]['create']; }
       return FALSE;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\KbRevision  $kbRevision
     * @return mixed
     */
    public function update(User $user, KbRevision $kbRevision) {
      /*if ($user->id == $rev->created_by) { return TRUE; }
      $access = DB::table('kbrev_permissions')
                   ->select('update')
                   ->where('uid', $user->id)
                   ->where('krid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['update']) { return $access[0]['update']; } }
      unset($access);
      $access = DB::table('kbrev_permissions')
                   ->select('update')
                   ->where('uid', $user->id)
                   ->where('krid', $rev->id)
                   ->get();
      if (count($access)) { return $access[0]['update']; }*/
      return FALSE;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\KbRevision  $kbRevision
     * @return mixed
     */
    public function delete(User $user, KbRevision $kbRevision) {
      if ($user->id == $rev->created_by) { return TRUE; }
      $access = DB::table('kbrev_permissions')
                   ->select('delete')
                   ->where('uid', $user->id)
                   ->where('krid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['delete']) { return $access[0]['delete']; } }
      unset($access);
      $access = DB::table('kbrev_permissions')
                   ->select('delete')
                   ->where('uid', $user->id)
                   ->where('krid', $rev->id)
                   ->get();
      if (count($access)) { return $access[0]['delete']; }
      return FALSE;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\KbRevision  $kbRevision
     * @return mixed
     */
    public function restore(User $user, KbRevision $kbRevision) {
      $access = DB::table('kbrev_permissions')
                   ->select('restore')
                   ->where('uid', $user->id)
                   ->where('krid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['restore']) { return $access[0]['restore']; } }
      unset($access);
      $access = DB::table('kbrev_permissions')
                   ->select('restore')
                   ->where('uid', $user->id)
                   ->where('krid', $rev->id)
                   ->get();
      if (count($access)) { return $access[0]['restore']; }
      return FALSE;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\KbRevision  $kbRevision
     * @return mixed
     */
    public function forceDelete(User $user, KbRevision $kbRevision) {
      $access = DB::table('kbrev_permissions')
                   ->select('forceDelete')
                   ->where('uid', $user->id)
                   ->where('krid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['forceDelete']) { return $access[0]['forceDelete']; } }
      unset($access);
      $access = DB::table('kbrev_permissions')
                   ->select('forceDelete')
                   ->where('uid', $user->id)
                   ->where('krid', $rev->id)
                   ->get();
      if (count($access)) { return $access[0]['forceDelete']; }
      return FALSE;
    }
}
