<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class UserPolicy {
    use HandlesAuthorization;

    public function before(User $user, $ability) {
        if ($user->admin) { return TRUE; }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user) {
      $access = DB::table('user_permissions')
                   ->select('viewAny')
                   ->where('uid', $user->id)
                   ->get();
      if (count($access)) { return $access[0]['viewAny']; }
      return FALSE;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function view(User $user, User $model) {
      $access = DB::table('user_permissions')
                   ->select('viewAny')
                   ->where('uid', $user->id)
                   ->get();
      if (count($access)) { return $access[0]['viewAny']; }
      return FALSE;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user) {
      $access = DB::table('user_permissions')
                   ->select('create')
                   ->where('uid', $user->id)
                   ->get();
      if (count($access)) { return $access[0]['create']; }
       return FALSE;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function update(User $user, User $model) {
      if ($model->admin) { return FALSE; }
      $access = DB::table('user_permissions')
                   ->select('update')
                   ->where('uid', $user->id)
                   ->get();
      if (count($access)) { return $access[0]['update']; }
      return FALSE;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model) {
      if ($model->admin) { return FALSE; }
      $access = DB::table('user_permissions')
                   ->select('delete')
                   ->where('uid', $user->id)
                   ->get();
      if (count($access)) { return $access[0]['delete']; }
      return FALSE;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model) {
      $access = DB::table('user_permissions')
                   ->select('restore')
                   ->where('uid', $user->id)
                   ->get();
      if (count($access)) { return $access[0]['restore']; }
      return FALSE;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model) {
      $access = DB::table('user_permissions')
                   ->select('forceDelete')
                   ->where('uid', $user->id)
                   ->get();
      if (count($access)) { return $access[0]['forceDelete']; }
      return FALSE;
    }
}
