<?php

namespace App\Policies;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy {
    use HandlesAuthorization;

    public function before(User $user, $ability) {
        if ($user->admin) { return TRUE; }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user) {
      $access = DB::table('ticket_permissions')
                   ->select('viewAny')
                   ->where('uid', $user->id)
                   ->where('tkid', NULL)
                   ->get();
      if (count($access)) { return $access[0]['viewAny']; }
      return TRUE;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Ticket  $ticket
     * @return mixed
     */
    public function view(User $user, Ticket $ticket) {
      $access = DB::table('ticket_permissions')
                   ->select('view')
                   ->where('uid', $user->id)
                   ->where('tkid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['view']) { return $access[0]['view']; } }
      unset($access);
      $access = DB::table('ticket_permissions')
                   ->select('view')
                   ->where('uid', $user->id)
                   ->where('tkid', $ticket->id)
                   ->get();
      if (count($access)) { return $access[0]['view']; }
      return TRUE;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user) {
      $access = DB::table('ticket_permissions')
                   ->select('create')
                   ->where('uid', $user->id)
                   ->where('tkid', NULL)
                   ->get();
      if (count($access)) { return $access[0]['create']; }
       return FALSE;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Ticket  $ticket
     * @return mixed
     */
    public function update(User $user, Ticket $ticket) {
      $access = DB::table('ticket_permissions')
                   ->select('update')
                   ->where('uid', $user->id)
                   ->where('tkid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['update']) { return $access[0]['update']; } }
      unset($access);
      $access = DB::table('ticket_permissions')
                   ->select('update')
                   ->where('uid', $user->id)
                   ->where('tkid', $ticket->id)
                   ->get();
      if (count($access)) { return $access[0]['update']; }
      return FALSE;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Ticket  $ticket
     * @return mixed
     */
    public function delete(User $user, Ticket $ticket) {
      $access = DB::table('ticket_permissions')
                   ->select('delete')
                   ->where('uid', $user->id)
                   ->where('tkid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['delete']) { return $access[0]['delete']; } }
      unset($access);
      $access = DB::table('ticket_permissions')
                   ->select('delete')
                   ->where('uid', $user->id)
                   ->where('tkid', $ticket->id)
                   ->get();
      if (count($access)) { return $access[0]['delete']; }
      return FALSE;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Ticket  $ticket
     * @return mixed
     */
    public function restore(User $user, Ticket $ticket) {
      $access = DB::table('ticket_permissions')
                   ->select('restore')
                   ->where('uid', $user->id)
                   ->where('tkid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['restore']) { return $access[0]['restore']; } }
      unset($access);
      $access = DB::table('ticket_permissions')
                   ->select('restore')
                   ->where('uid', $user->id)
                   ->where('tkid', $ticket->id)
                   ->get();
      if (count($access)) { return $access[0]['restore']; }
      return FALSE;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Ticket  $ticket
     * @return mixed
     */
    public function forceDelete(User $user, Ticket $ticket) {
      $access = DB::table('ticket_permissions')
                   ->select('forceDelete')
                   ->where('uid', $user->id)
                   ->where('tkid', NULL)
                   ->get();
      if (count($access)) { if ($access[0]['forceDelete']) { return $access[0]['forceDelete']; } }
      unset($access);
      $access = DB::table('ticket_permissions')
                   ->select('forceDelete')
                   ->where('uid', $user->id)
                   ->where('tkid', $ticket->id)
                   ->get();
      if (count($access)) { return $access[0]['forceDelete']; }
      return FALSE;
    }
}
