<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected $namespace = 'App\\Http\\Controllers';
    protected $wckdomain = "wcksoft.wck";

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot() {
        $this->configureRateLimiting();

        $this->routes(function () {
          // API
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));
          // admin
            Route::prefix('admin')
                ->middleware('admin')
                ->namespace($this->namespace)
                ->group(base_path('routes/admin.php'));
          // hosting
            Route::prefix('hosting')
                ->middleware('hosting')
                ->namespace($this->namespace)
                ->group(base_path('routes/hosting.php'));
          // software
            Route::prefix('software')
                ->middleware('software')
                ->namespace($this->namespace)
                ->group(base_path('routes/software.php'));
          // tos
            Route::prefix('tos')
                ->middleware('tos')
                ->namespace($this->namespace)
                ->group(base_path('routes/tos.php'));
          // privacy
            Route::prefix('privacy')
                ->middleware('privacy')
                ->namespace($this->namespace)
                ->group(base_path('routes/privacy.php'));
          // support
            Route::prefix('support')
                ->middleware('support')
                ->namespace($this->namespace)
                ->group(base_path('routes/support.php'));
          // KB
            Route::prefix('kb')
                ->middleware('kb')
                ->namespace($this->namespace)
                ->group(base_path('routes/kb.php'));
          // blog
            Route::prefix('blog')
                ->middleware('blog')
                ->namespace($this->namespace)
                ->group(base_path('routes/blog.php'));
          // news
            Route::prefix('news')
                ->middleware('news')
                ->namespace($this->namespace)
                ->group(base_path('routes/news.php'));
          // sales
            Route::prefix('sales')
                ->middleware('sales')
                ->namespace($this->namespace)
                ->group(base_path('routes/sales.php'));
          // forum
            Route::prefix('forum')
                ->middleware('forum')
                ->namespace($this->namespace)
                ->group(base_path('routes/forum.php'));
          // community
            Route::prefix('community')
                ->middleware('community')
                ->namespace($this->namespace)
                ->group(base_path('routes/community.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting() {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
