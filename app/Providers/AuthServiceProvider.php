<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider {
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        App\Models\Blog::class => App\Policies\BlogPolicy::class,
        App\Models\BlogPost::class => App\Policies\BlogPostPolicy::class,
        App\Models\Forum::class => App\Policies\ForumPolicy::class,
        App\Models\ForumTopic::class => App\Policies\ForumTopicPolicy::class,
        App\Models\ForumPost::class => App\Policies\ForumPostPolicy::class,
        App\Models\KbArticle::class => App\Policies\KbPolicy::class,
        App\Models\KbRevision::class => App\Policies\KbRevPolicy::class,
        App\Models\News::class => App\Policies\NewsPolicy::class,
        App\Models\Product::class => App\Policies\ProductPolicy::class,
        App\Models\Ticket::class => App\Policies\TicketPolicy::class,
        App\Models\User::class => App\Policies\UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
