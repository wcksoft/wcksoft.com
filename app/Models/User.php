<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class User extends Authenticatable {
    use HasFactory;
    use Notifiable;
    use UuidModelTrait;

    protected $fillable = ['firstname', 'lastname', 'email', 'password', 'admin', 'picture', 'deleted_by', 'updated_by', 'created_by'];
    protected $hidden = ['password', 'remember_token',];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    protected $casts = ['email_verified_at' => 'datetime',];

    public function blogs() { return $this->hasMany('Blog', 'created_by'); }
    public function blogPosts() { return $this->hasMany('BlogPost', 'created_by'); }
    public function forums() { return $this->hasMany('Forum', 'created_by'); }
    public function forumTopics() { return $this->hasMany('ForumTopic', 'created_by'); }
    public function forumPosts() { return $this->hasMany('Forum', 'created_by'); }
}
