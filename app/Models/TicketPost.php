<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class TicketPost extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['uid','tid','post','created_by','updated_by','deleted_by'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function ticket() { return $this->hasOne('Ticket', 'id', 'tid'); }
}
