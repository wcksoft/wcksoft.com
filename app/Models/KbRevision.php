<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class KbRevision extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $table = 'kb_revisions';
    protected $fillable = ['title','kbid','title','article','version','created_by','deleted_by','updated_by','published_by'];
    protected $dates = ['deleted_at','updated_at','created_at','published_at'];

    public function kb() { return $this->belongsTo('KbArticle', 'id', 'kbid'); }
     public function uploads() { return $this->hasMany('KbUpload', 'kbid', 'id'); }
}
