<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class KbCategory extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $table = 'kb_categories';
    protected $fillable = ['name','created_by','deleted_by','updated_by'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function articles() { return $this->belongsToMany('KbArticle'); }
}
