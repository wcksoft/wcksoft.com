<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class Blog extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['title','post','status','deleted_by','updated_by','created_by'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function creator() { return $this->belongsTo('User', 'created_by'); }
    public function posts() { return $this->hasMany('BlogPost', 'bid'); }
}
