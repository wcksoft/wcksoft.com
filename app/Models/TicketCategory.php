<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class TicketCategory extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['name'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function tickets() { return $this->hasMany('Ticket', 'tid', 'id'); }
}
