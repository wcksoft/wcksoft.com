<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class ForumUpload extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['name','fpid','created_by','deleted_by','updated_by','file'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function topic() { return $this->belongsTo('ForumPost', 'fpid'); }
}
