<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class News extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['title','post','important','created_by','updated_by','deleted_by'];
    protected $dates = ['deleted_at','updated_at','created_at'];
}
