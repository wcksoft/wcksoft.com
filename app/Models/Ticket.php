<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class Ticket extends Model {
  use HasFactory;
  use SoftDeletes;
  use UuidModelTrait;

  protected $fillable = ['tcid','kbid','title','description','closed_by','reopened_by','created_by','updated_by','deleted_by'];
  protected $dates = ['deleted_at','updated_at','created_at','closed_at','reopened_at'];

  public function kb() { return $this->belongsTo('KbArticle', 'id', 'ftid'); }
  public function category() { return $this->belongsTo('TicketCategory', 'id', 'tcid'); }
  public function posts() { return $this->hasMany('TicketPost', 'tid', 'id'); }
  public function uploads() { return $this->hasMany('TicketUpload', 'tid', 'id'); }
}
