<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class ForumPost extends Model {
    use HasFactory;
    use SoftDeletes, UuidModelTrait;

    protected $fillable = ['post','tid','created_by','deleted_by','updated_by','show','approved_by'];
    protected $dates = ['deleted_at','updated_at','created_at','approved_at'];


    public function creator() { return $this->belongsTo('User', 'created_by'); }
    public function topic() { return $this->belongsTo('ForumTopic', 'tid'); }
    public function uploads() { return $this->hasMany('ForumUpload', 'fpid'); }
    public function author() { return $this->belongsTo('User', 'created_by'); }
    public function deletedBy() { return $this->belongsTo('User', 'deleted_by'); }
    public function updatedBy() { return $this->belongsTo('User', 'updated_by'); }
    public function approvedBy() { return $this->belongsTo('User', 'approved_by'); }
    public function authorName() { return $this->author === NULL ? "System" : User::find($this->author)[0]->name; }

    public function getParents() {
      $data = array();
      if (!is_null($this->tid)) {
        $topic = $this->topic();
        $x = array();
        $x['id'] = $topic->id;
        $x['name'] = $topic->name;
        array_push($data, $x);
        if (!is_null($topic->fid)) {
          $parent = $this->forum();
          while (true) {
            $x = array();
            $x['id'] = $parent->id;
            $x['name'] = $parent->name;
            array_push($data, $x);
            if (is_null($parent->parent_id)) { break; }
            $parent = Forum::find($parent->parent_id);
          }
        }
      }
      return $data;
    }

    public function showLinks() {
      $data = array();
      if (!is_null($this->tid)) {
        $topic = Topic::find($this->tid);
        array_push($data, '<a href="'.url('/topics/'.$topic->id).'" class="">'.$topic->name.'</a>');
        $forum = Forum::find($topic->fid);
        while (true) {
          array_push($data, '<a href="'.url('/forums/'.$forum->id).'" class="">'.$forum->name.'</a>');
          if (is_null($forum->parent_id)) { break; }
          $forum = Forum::find($forum->parent_id);
        }
      }
      array_push($data, '<a href="'.url('/forums').'" class="">Forum home</a>');
      $string = "";
      for ($i = count($data)-1; $i >= 0; $i--) {
        if ($i == 0) {
          $string .= $data[$i];
        } else {
          $string .= $data[$i]." / ";
        }
      }
      return $string;
    }
}
