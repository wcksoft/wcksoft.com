<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Alsofronie\Uuid\UuidModelTrait;
use Carbon\Carbon;

class ForumTopic extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['title','fid','created_by','deleted_by','updated_by','pinned','important','locked_by'];
    protected $dates = ['deleted_at','updated_at','created_at','locked_at'];

    public function creator() { return $this->belongsTo('User', 'created_by'); }
    public function forum() { return $this->hasOne('Forum', 'fid'); }
    public function posts() { return $this->hasMany('ForumPost', 'tid'); }
    public function kb() { return $this->belongsTo('KBArticle', 'id', 'kbid'); }
    public function posts() { return $this->hasMany('ForumPost', 'tid'); }
    public function user() { return $this->hasManyThrough('User', 'Post', 'created_by', 'id', 'id', 'id'); }
    //public function permissions() { return $this->hasMany('App\TopicPermission', 'tid'); }
    public function forum() { return $this->belongsTo('Forum', 'fid'); }
    public function author() { return $this->belongsTo('User', 'created_by'); }
    public function deletedBy() { return $this->belongsTo('User', 'deleted_by'); }
    public function updatedBy() { return $this->belongsTo('User', 'updated_by'); }
    public function lockedBy() { return $this->belongsTo('User', 'locked_by'); }
    public function lastestPostAuthor() { return $this->posts->last()->created_by === NULL ? "System" : User::find($this->posts->last()->created_by)->name; }
    public function lastestPostTime() { return $this->posts->last()->created_at === NULL ? "Unknown" : $this->posts->last()->created_at; }
    public function authorName() { return $this->author === NULL ? "System" : User::find($this->author)[0]->name; }

    public function lock() {
      $this->locked_at = Carbon::now();
      if (Auth::check()) { $this->locked_by = Auth::user()->id; }
      $this->save();
    }

    public function unlock() {
      $this->locked_at = NULL;
      $this->locked_by = NULL;
      $this->save();
    }

    public function pin() {
      $this->pinned = TRUE;
      $this->save();
    }

    public function unpin() {
      $this->pinned = FALSE;
      $this->save();
    }

    public function flag() {
      $this->important = TRUE;
      $this->save();
    }

    public function unflag() {
      $this->important = FALSE;
      $this->save();
    }

    public function getParents() {
      $data = array();
      if (!is_null($this->tid)) {
        $parent = $this->forum();
        while (true) {
          $x = array();
          $x['id'] = $parent->id;
          $x['name'] = $parent->name;
          array_push($data, $x);
          if (is_null($parent->parent_id)) { break; }
          $parent = Forum::find($parent->parent_id);
        }
      }
      return $data;
    }

    public function showLinks() {
      $data = array();
      if (!is_null($this->fid)) {
        $forum = Forum::find($this->fid);
        while (true) {
          array_push($data, '<a href="'.url('/forums/'.$forum->id).'" class="">'.$forum->name.'</a>');
          if (is_null($forum->parent_id)) { break; }
          $forum = Forum::find($forum->parent_id);
        }
      }
      array_push($data, '<a href="'.url('/forums').'" class="">Forum home</a>');
      $string = "";
      for ($i = count($data)-1; $i >= 0; $i--) {
        if ($i == 0) {
          $string .= $data[$i];
        } else {
          $string .= $data[$i]." / ";
        }
      }
      return $string;
    }
}
