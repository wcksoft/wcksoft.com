<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class Product extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['pcid','name','description','created_by','updated_by','deleted_by'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function kb() { return $this->belongsTo('KbArticle', 'id', 'pid'); }
     public function category() { return $this->belongsTo('ProductCategory', 'id', 'pcid'); }
}
