<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class KbArticle extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $table = 'kb_articles';
    protected $fillable = ['kbcid','pid','ftid','tid','created_by','updated_by','deleted_by'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function categories() { return $this->belongsToMany('KbCategory', 'kb_articles', 'id', 'kbcid'); }
    public function revisions() { return $this->hasMany('KbRevision', 'kbid', 'id'); }
    public function topics() { return $this->hasMany('ForumTopic', 'ftid', 'id'); }
    public function tickets() { return $this->hasMany('Ticket', 'tid', 'id'); }
    public function products() { return $this->hasMany('Product', 'pid', 'id'); }
}
