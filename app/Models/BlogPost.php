<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

class BlogPost extends Model {
    use HasFactory;
    use SoftDeletes;
    use UuidModelTrait;

    protected $fillable = ['bid', 'post', 'deleted_by', 'updated_by', 'created_by'];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];

    public function creator() { return $this->belongsTo('User', 'created_by'); }
    public function blog() { return $this->belongsTo('Blog', 'bid'); }
}
