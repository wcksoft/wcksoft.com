# Wicked Software Corporation

Wicked Software is an IT service company, that offers hosting, software and consulting.

## Version
2021.01

## Subdomains list
```
admin, api, blog, forum, hosting, irc, kb *new*, news, privacy, sales, software, support, tos, www
```

## Breakdown

Breakdown of each sub-site.

### admin.wcksoft.com

* users: *add, delete, edit, permissions*
* stats: *traffic, support KPI, IRC data, sales performance*
* sales: *add, delete, edit, list*
* hosting: *plans, available systems, customers*
* forum: *settings, moderator panel*

### api.wcksoft.com

This just needs to feed the required data for our vue frontends.
**This needs to be done as we develop features that will be using it.**

* news: *add/list/view/delete*
* sales: *list*

### blog.wcksoft.com

This can be designed later on as it isn't a critical component.

### forum.wcksoft.com

This can be designed later, but before blog.

### hosting.wcksoft.com

* landing page
* self-serve: *buy, manage, renew*
* hosting options: *docker, lxc, kvm*

### irc.wcksoft.com

This can be designed at the very end.

### kb.wcksoft.com

Knowledge base.

### news.wcksoft.com

This should be one of the first thing to be designed.

* user access: *publisher*
* Mosaic UI homepage.
* pages: *home, add, edit, list*

### privacy.wcksoft.com

Pretty straight forward and already completed. *needs review*

### sales.wcksoft.com

Here we have the promotions, each on their own pages under the subdomain.

### software.wcksoft.com

This is the software/web designer's portion of the website.

* rates: *per hour, fixed rate per project*
* Detail all the skills of the programmers on the team.

### support.wcksoft.com

Ticketing software.

### tos.wcksoft.com

Pretty straight forward and already completed. *needs review*

### www.wcksoft.com

Main page.
